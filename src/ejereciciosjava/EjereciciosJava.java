package ejereciciosjava;

import java.util.Arrays;
import java.util.StringTokenizer;

/**
 *
 * @author Alberto Goujon
 */
public class EjereciciosJava 
{

    public int[] maximo(int[] listaNumeros) {
        int[] numerosMaximo = {0, 0};                                           //Para poder guardar los numeros máximos

        for (int i = 0; i < listaNumeros.length; i++)                           //Recorremos el array.
        {
            if (numerosMaximo[0] < listaNumeros[i])                     
            {
                numerosMaximo[1] = numerosMaximo[0];                            //El primer numero maximo lo guardamos en el numeroMaximo [0].
                numerosMaximo[0] = listaNumeros[i];                             //Y este para poderse actualizar comprueba si tiene alguno mayor.
            } 
            else if (numerosMaximo[1] < listaNumeros[i]) 
            {
                numerosMaximo[1] = listaNumeros[i];                             //Aqui hacemos un poco mas de lo mismo, solo que no es necesario que estemos comprobando uno a uno porque:
                                                                                //Directamente lo vamos actulizando respecto a que sea mayor que el anterior numero guandado en  numerosMaximos[1]
            }
        }

        return numerosMaximo;
    }

    public boolean esPalindromo(String frase)
    {
        boolean palindromo = true;
        String fraseSimple = frase.toUpperCase();				//Primero comprobamos quitamos los posibles errores. 
        fraseSimple = fraseSimple.replaceAll("Á", "A");				//Para ello lo pasamos todo a mayúsculas,
        fraseSimple = fraseSimple.replaceAll("Ä", "A");				//Quitamos acentos y reemplazamos la 'Ñ'.

        fraseSimple = fraseSimple.replaceAll("É", "E");
        fraseSimple = fraseSimple.replaceAll("Ë", "E");

        fraseSimple = fraseSimple.replaceAll("Í", "I");
        fraseSimple = fraseSimple.replaceAll("Ï", "I");

        fraseSimple = fraseSimple.replaceAll("Ó", "O");
        fraseSimple = fraseSimple.replaceAll("Ö", "O");

        fraseSimple = fraseSimple.replaceAll("Ú", "U");
        fraseSimple = fraseSimple.replaceAll("Ü", "U");

        fraseSimple = fraseSimple.replaceAll("Ñ", "n");

        fraseSimple = fraseSimple.replaceAll("\\W", "");                        //Ahora quitamos el resto de símbolos que no sean letras. 

        for (int i = 0; i < fraseSimple.length() / 2; i++)                      //Aquí recorremos el Array haciendo las comprobaciones.
        {                                                                       
            if (fraseSimple.charAt(i) != fraseSimple.charAt
            (fraseSimple.length() - i - 1) && palindromo == true) 
            {
                palindromo = false;
            }
        }
        return palindromo;
    }

    public boolean esIsograma(String palabra)
    {      
        for(int i = 0; i < palabra.length(); i++)                               //Recorro la  palabra.
        {
            for (int j = i + 1; j < palabra.length(); j++)                      //lo hago para el conteo de letras repetidas.
            {
                if (palabra.charAt(i) == palabra.charAt(j))                     //si se encuentra una letra igual que en el primer for entrara en el return false.
                {
                   return false;
                }
            }
        }
        return true;                                                            //si no pasa nada no es un isograma.
    }
    public int calendario (int dias)
    {
        for(int i = 0 ; i < dias; i ++)                                         //Para el numero de XX segun el mes que escoja.
        {
            System.out.print("XX");                                             //Imprime las XX.
            System.out.print(" ");                                              //Impprime los espacios entre XX y XX.
        }
        
        int diasSemanales = dias;                                               //Igualamos los dias semanales ("XX") con los dias (numeros) para que los contadores esten en el mismo valor.
        
        for(int i = 1; i < 31; i++)                                             //Para los dias de int.
        {
            
            System.out.print(i);                                                //Imprime el 1, 2, 3 ,4...
            System.out.print(" ");                                              //Imprime el espacio entre los numeros.
            diasSemanales++;                                                    //hace que del 1 pase al 2, del 2 al 3 y asi sucesivamente.
            if(diasSemanales%7 == 0)                                            //Por linea solo se imprime 7 elementos.
            {
                System.out.println();                                           //Al llegrar al sexto elemento hace un salto a la siguiente línea.
            }
        }
        return 31;                                                              //Devuelve 31 porque he puesto que sea < 31.
               
    }
    
    public boolean esAnagrama (String anagrama, String anagrama2)
    {
        char[] ordenada1 = anagrama.toCharArray();                              //Lo convierto a array para poder luego comparar.
        java.util.Arrays.sort(ordenada1);                                       //Se encargará de ordenar cualquier tipo de array.
        String cadena1 = new String(ordenada1);                                 //Lo hacemos en una nueva string de manera ordenada para que luego lo pueda comparar con otros elementos.
             
        char[] ordenada2 = anagrama2.toCharArray();                             //Lo convierto a array para poder luego comparar.
        java.util.Arrays.sort(ordenada2);                                       //Se encargará de ordenar cualquier tipo de array.
        String cadena2 = new String(ordenada2);                                 //Lo hacemos en una nueva string de manera ordenada para que luego lo pueda comparar con otros elementos.
        
        
        if(cadena1.equals(cadena2))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    public String acronimo (String acronimo)
    {
        String quitaDe = acronimo.toUpperCase();                                //Primero quitamos todos los casosy de paso lo convertimos todo a mayúsculas:
        
        quitaDe = quitaDe.replaceAll("DE ", " ");                               //El caso del DE.
        quitaDe = quitaDe.replaceAll("Y ", " ");                                //El caso del Y.
        quitaDe = quitaDe.replaceAll("E ", " ");                                //El caso del E.
        quitaDe = quitaDe.replaceAll("LAS ", " ");                              //El caso del LAS.
        quitaDe = quitaDe.replaceAll("LA ", " ");                               //El caso del LA.
        
        StringTokenizer stPalabras = new StringTokenizer(quitaDe);              // Hacemos subcadenas para poder quedarnos solo con la primera letra de cada plabra.
        
       while (stPalabras.hasMoreTokens())                                       //Recorro dicho StringTokenizer para analizar cada una de las palabras y quedarnos con su primera letra.
       {
            String sPalabra = stPalabras.nextToken();                           //Cada uno de los token devueltos por .nextToken() representa una de las palabras de la frase.
            System.out.print(sPalabra.substring(0,1));                          //Y para terminar si o si necesitas hacer una subtracción de la subcadena. El 0 y el uno hace que siempre sea el de la primera letra.
       }
        return "";
 
    }
     public boolean escaleraDePalabras (char[][] escalera)
     {
         boolean esEscalera = true;
         int letrasDiferentes;
         for (int i = 0; i < escalera.length - 1; i++)                          //Recorro toda a palabra
         {		
	    letrasDiferentes = 0;						//Establecemos las letras diferentes a 0 para asi despues cuando se ponga en uno podamos devolver false
	    if (escalera[i].length == escalera[i + 1].length)                   //Ahora solo compararp si la longitud de las palabras es la misma
            {		
		for (int j = 0; j < escalera[i].length; j++) 
                {
		    if (escalera[i][j] != escalera[i + 1][j])                   //Si alguna letra ha sido diferente se sumara a 1 letrasDiferentes 
                    {
			letrasDiferentes++;
		    }
		    if (letrasDiferentes > 1) {					//Si difieren mas de una letra, entonces no es una escalera
			esEscalera = false;
		    }
		}
	    } else {
		System.out.println("Las palabras no tienen la misma longitud"); //En caso de que pongan mal las longitudes de palabras se añadira este texto para el usuario
                return false;
	    }
	}
         return esEscalera;
     }
     
     private int costeErroresADN (String uno, String dos)
     {
        int fallosNoCasando = 0;                                                //Iniciamos  los fallos a 0
        if (uno.length() == dos.length())                                       //En caso que no sea iguales de longitudes nunca entrara
        {
	   for (int i = 0; i < uno.length(); i++) 
           {
               fallosNoCasando += costoDeFallosNucleotidos(uno.charAt(i), dos.charAt(i));
	   }
	}
        else
        {
           System.out.println("Los ADN no tienen la misma longitud"); 
        }
	return fallosNoCasando;
     }
     
     private int costoDeFallosNucleotidos(char a, char b)
     {
        int erroresNucleotipos = 0;                                             //Inicio el contador a 0 de erroresNucleotipos
         
        if (a == b)                                                 //Este es para ver si son iguales
        {
	   erroresNucleotipos++;
           return erroresNucleotipos;
	}
        
        if(a == '-' || b == '-')                                                //Estoy comprobando que una de las letras no tenga pareja en los dos ADNS 
        {
            erroresNucleotipos = 2;
            return erroresNucleotipos;
        }
        
        //Para el resto de comprobaciones transforma las 'A' en 'T'-------------
        if(a == 'A')
        {
            a = 'T';
        }
        
        if(b == 'A')
        {
            b = 'T';
        }
        //----------------------------------------------------------------------
        
        //Para el resto de comprobaciones transforma las 'G' en 'C'-------------
        if(a == 'G')
        {
            a = 'C';
        }
        
        if(b == 'G')
        {
            b = 'C';
        }
        //----------------------------------------------------------------------
        return erroresNucleotipos;
     }
     
     
     
     
     
     

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EjereciciosJava ejercicio1 = new EjereciciosJava();
        //ejercicio1.ejemplosString();
        
//-----------------------------------Ejercicio 1 basicos Nivel 1---------------------------------------------------------------------
        System.out.println("Ejercicio 1 Nivel 1");
        int[] listaNumeros = {50, 31, 27, 2, 5, 99};
        int[] listaNumeros2 = {150, 31, 27, 2, 5, 99};
        System.out.println(Arrays.toString(ejercicio1.maximo(listaNumeros)));
        System.out.println(Arrays.toString(ejercicio1.maximo(listaNumeros2)));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
//-----------------------------------Ejercicio 2 basicos Nivel 1---------------------------------------------------------------------
        System.out.println("Ejercicio 2 Nivel 1");
        String palindromo = "ANA";
        String palindromo2 = "DABALE ARROZ A LA ZORRA EL ABAD";
        String palindromo3 = "Allí por la tropa portado, traído a ese	paraje de maniobras, una tipa como capitán usar boina me dejara,pese a odiar toda tropa	por tal	ropilla";
        String palindromo4 = "hola";
        System.out.println(ejercicio1.esPalindromo(palindromo));
        System.out.println(ejercicio1.esPalindromo(palindromo2));
        System.out.println(ejercicio1.esPalindromo(palindromo3));
        System.out.println(ejercicio1.esPalindromo(palindromo4));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
// -----------------------------------Ejercicio 3 basicos Nivel 1---------------------------------------------------------------------
        System.out.println("Ejercicio 3 Nivel 1");
        String isograma  = "HOLA";
        String isograma2 = "OSO";
        String isograma3 = "Electroencefalografista";
        System.out.println(ejercicio1.esIsograma(isograma));
        System.out.println(ejercicio1.esIsograma(isograma2));
        System.out.println(ejercicio1.esIsograma(isograma3));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
// -----------------------------------Ejercicio 4 basicos Nivel 1----------------------------------------------------------------------
        System.out.println("Ejercicio 4 Nivel 1");
        System.out.println(ejercicio1.calendario(4));
        System.out.println("=========================================================");
        System.out.println("=========================================================");        
// -----------------------------------Ejercicio 5 basicos Nivel 1----------------------------------------------------------------------
        System.out.println("Ejercicio 5 Nivel 1");
        String anagrama1 = "roma";
        String anagrama2 = "amor";
        String anagrama3 = "Amor";
        String anagrama4 = "mora";
        String anagrama5 = "m ra";
        System.out.println(ejercicio1.esAnagrama(anagrama1, anagrama2));
        System.out.println(ejercicio1.esAnagrama(anagrama1, anagrama3));
        System.out.println(ejercicio1.esAnagrama(anagrama1, anagrama4));
        System.out.println(ejercicio1.esAnagrama(anagrama1, anagrama5));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
// -----------------------------------Ejercicio 6 basicos Nivel 1----------------------------------------------------------------------
        System.out.println("Ejercicio 6 Nivel 1");
        String acroniimo1 = "Alta Velocidad Española";
        String acroniimo2 = "Objeto Volador No Identificado ";
        String acroniimo3 = "Tecnología de la Información y de las Comunicaciones";
        String acroniimo4 = "United Nations International Children’s Emergency Fund";
        String acroniimo5 = "Unión Europea";
        String acroniimo6 = "europe union";
        System.out.println(ejercicio1.acronimo(acroniimo1));
        System.out.println(ejercicio1.acronimo(acroniimo2));
        System.out.println(ejercicio1.acronimo(acroniimo3));
        System.out.println(ejercicio1.acronimo(acroniimo4));
        System.out.println(ejercicio1.acronimo(acroniimo5));
        System.out.println(ejercicio1.acronimo(acroniimo6));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
 // -----------------------------------Ejercicio 1 basicos Nivel 2----------------------------------------------------------------------
        System.out.println("Ejercicio 1 Nivel 2");
        char [][] listaPalabras = 
        {
            {'P', 'A', 'T', 'A'},
            {'P', 'A', 'T', 'O'},
            {'R', 'A', 'T', 'O'},
            {'R', 'A', 'M', 'O'},
            {'G', 'A', 'M', 'O'},
            {'G', 'A', 'T', 'O'},
            {'M', 'A', 'T', 'O'},
          
        };
        char [][] listaPalabras2 = 
        {
            {'P', 'A', 'T', 'A'},
            {'P', 'O', 'T', 'O'},
            {'R', 'A', 'T', 'O'},
            {'R', 'A', 'M', 'O'},
            {'G', 'A', 'M', 'O'},
            {'G', 'A', 'T', 'O'},
            {'M', 'A', 'T', 'O'},
          
        };
        char [][] listaPalabras3 = 
        {
            {'P', 'A', 'T', 'A'},
            {'P', 'A', 'T', 'A'},
            {'R', 'A', 'T', 'A'},
            {'R', 'A', 'M', 'A'},
            {'G', 'A', 'M', 'A'},
            {'G', 'A', 'T', 'A'},
            {'M', 'A', 'T', 'A'},
          
        };
        char [][] listaPalabras4 = 
        {
            {'H', 'O', 'L', 'A'},
            {'P', 'A', 'L', 'O'},
            {'P', 'A', 'T', 'A'},
            {'R', 'A', 'T', 'A'},
            {'A', 'T', 'A', 'R'},
            {'H', 'A', 'R', 'A'},
            {'M', 'A', 'R', 'A'},
          
        };
         char [][] listaPalabras5 = 
        {
            {'P', 'A', 'T', 'A', 'A'},
            {'P', 'A', 'T', 'O'},
            {'R', 'A', 'T', 'O'},
            {'R', 'A', 'M', 'O'},
            {'G', 'A', 'M', 'O'},
            {'G', 'A', 'T', 'O'},
            {'M', 'A', 'T', 'O'},
          
        };
        System.out.println(ejercicio1.escaleraDePalabras(listaPalabras));    
        System.out.println(ejercicio1.escaleraDePalabras(listaPalabras2));
        System.out.println(ejercicio1.escaleraDePalabras(listaPalabras3));
        System.out.println(ejercicio1.escaleraDePalabras(listaPalabras4));
        System.out.println(ejercicio1.escaleraDePalabras(listaPalabras5));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
        //-------------------------------Ejercicio 1 basicos Nivel 2----------------------------------------------------------------------
        System.out.println("Ejercicio 2 Nivel 2");
           String ADN1 = "ACGT";
	    String ADN2 = "TGCA";
	    String ADN3 = "A-C-G-T-ACGT";
	    String ADN4 = "TTGGCCAATGCA";
	    String ADN5 = "AAAAAAAA";
	    String ADN6 = "TTTATTTT";
	    String ADN7 = "GATTACA";
	    String ADN8 = "CTATT-T";
	    String ADN9 = "CAT-TAG-ACT";
	    String ADN10 = "GTATATCCAAA";
	    String ADN11 = "--------";
	    String ADN12 = "ACGTACGT";
	    String ADN13 = "TAATAA";
	    String ADN14 = "ATTATT";
	    String ADN15 = "GGGA-GAATATCTGGACT";
	    String ADN16 = "CCCTACTTA-AGACCGGT";
        
        System.out.println(ejercicio1.costeErroresADN(ADN1, ADN2));
        System.out.println(ejercicio1.costeErroresADN(ADN3, ADN4));
        System.out.println(ejercicio1.costeErroresADN(ADN5, ADN6));
        System.out.println(ejercicio1.costeErroresADN(ADN7, ADN8));
        System.out.println(ejercicio1.costeErroresADN(ADN9, ADN10));
        System.out.println(ejercicio1.costeErroresADN(ADN11, ADN12));
        System.out.println(ejercicio1.costeErroresADN(ADN13, ADN14));
        System.out.println(ejercicio1.costeErroresADN(ADN15, ADN16));
        System.out.println("=========================================================");
        System.out.println("=========================================================");
    }
}