/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejereciciosjava;

/**
 *
 * @author Alberto
 */
public class EjercicosString 
{ 
        public void ejemplosString() {
        String cadena1 = "Acaso hubo buhos aca";

        System.out.println(cadena1.charAt(7));
        System.out.println(cadena1.charAt(0));
        System.out.println(cadena1.charAt(cadena1.length() - 1));

        System.out.println(cadena1.substring(11, 16));
        System.out.println(cadena1.substring(17));

        String[] arrayPalabras = cadena1.split(" ");
        System.out.println(arrayPalabras[2]);

        System.out.println(cadena1.indexOf("buho"));
        System.out.println(cadena1.indexOf('h'));
    }
    
}
